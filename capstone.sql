SELECT customerName FROM customers WHERE country = "Philippines";
SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";
SELECT productName, MSRP FROM products WHERE productName = "The Titanic";
SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";
SELECT customerName, state FROM customers WHERE state IS NULL;
SELECT firstName, lastName, email FROM employees WHERE firstName = "Steve" AND lastName = "Patterson";
SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000;
SELECT customerNumber, orderNumber, comments FROM orders WHERE comments LIKE "%DHL%";
SELECT productLine FROM productlines WHERE textDescription LIKE "%state of the art%";
SELECT DISTINCT country FROM customers;
SELECT DISTINCT status FROM orders;
SELECT customerName, country FROM customers WHERE country IN ("USA", "France", "Canada");
SELECT offices.city, firstName, lastName FROM offices
	JOIN employees ON offices.officeCode = employees.officeCode WHERE country = "Japan";
SELECT customers.customerName, employees.firstName, employees.lastName FROM employees
	JOIN customers ON employeeNumber = salesRepEmployeeNumber WHERE employees.firstName = "Leslie" AND employees.lastName = "Thompson";
SELECT products.productName, customers.customerName FROM customers
	JOIN orders ON customers.customerNumber = orders.customerNumber
	JOIN orderdetails ON orders.orderNumber = orderdetails.orderNumber
	JOIN products ON orderdetails.productCode = products.productCode WHERE customers.customerName = "Baane Mini Imports";
SELECT customers.customerName, customers.country, employees.firstName, employees.lastName, offices.country FROM employees
	JOIN customers ON employees.employeeNumber = customers.salesRepEmployeeNumber
	JOIN offices ON offices.officeCode = employees.officeCode WHERE customers.country = offices.country;
SELECT productName, quantityInStock, productLine FROM products WHERE productLine = "Planes" AND quantityInStock < 1000;
SELECT customerName, phone FROM customers WHERE phone LIKE "+81%";




